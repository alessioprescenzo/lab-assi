
# ProjectAdvisor (tmp)

Il sito si propone di facilitare il lavoro di gruppo mettendo a disposizione la possibilità di crearne di nuovi o aggiungersi ad esistenti.

Lo scopo ultimo del sito è dunque quello di fornire valutazioni ad utenti per verificarne la validità come eventuale membro di un gruppo.

## **Descrizione dati gestiti**

* Utente
  
      - Dati anagrafici: Nome, Cognome, Data e luogo di nascita, Sesso
      - Skills: capacità apprese
      - e-Mail: identificatore
      - Progetti: Lista con valutazione      
      - Valutazioni personali
  
* Progetto
      
      - Identificatore
      - Descrizione
      - Partecipanti: Lista utenti
      - Link al progetto: Git - Programmazione, Drive - Fotografia, ecc...
      - Comunicazione interna partecipanti: Sistema di mailboxing
      - Vincoli di partecipazione: Lista di requisiti
      - Valutazione progetto
      - Valutazione partecipanti nel progetto: valutazione da parte di membri
      - Commenti utenti esterni: Domande e proposte
  
* Valutazioni:
  
  * Progetto:
    
        - Punteggio: Media dei punteggi ottenuti dal progetto
        - Valutazione descrittiva dei componenti
        - Valutazione utente esterno        

  * Utente: 
        
        - Valutazione generale: Media dei punteggi ottenuti dall'utente
        - Valutazione da terzi
        - Valutazione per ogni progetto

# 

## **Funzionalità offerte agli utenti**

* Registrazione e gestione profilo
      
  
    Possibilità per l'utente di registrarsi al sito e di gestire le proprie informazioni personali
  
* Registrazione tramite OAuth

      Possibilità per l'utente di registrarsi tramite una piattaforma esterna attraverso OAuth

* Creazione e gestione progetto

      Possibilità per l'utente di creare un progetto, inserire vincoli di partecipazione, aggiungere e/o eliminare membri allo stesso

* Valutazione utente e progetto

      Possibilità per l'utente di valutare i propri colleghi e/o il progetto a cui ha partecipato

* Scambio messaggi fra membri di un progetto

      Possibilità per gli utenti di comunicare internamente al progetto 

#

## **User Stories**

1. View Projects as a non user:

        As a non-user 
        I want to access the Project's database
        So that i need to create an account

2. Registration:
        
        As a non-user
        I want to add data to the user's  database
        So that i can register my user
        
3. Remove personal account:

        As a user
        I want to remove data from the user's database
        So that i can remove my user

4. Modify user personal-data:
        
        As a user
        I want to modify data in the user's database
        So that i can modify my user information

5. Modify skills:
  
        As a user
        I want to modify data in the user's database
        So that i can modify my skills in my user's 

6. Add a user-wide evaluation:
  
        As a user
        I want to add data to another user's database
        So that i can add an evaluation to a user

7. View Personal info:        

        As a user
        I want to access the user's database
        So that i can view my profile



8. Ban a site user:

        As a super-user
        I want to add data to the database
        So that i can blacklist a user from the site

9.  View Projects as a user:

        As a user
        I want to access the project's database
        So that i can view the projects

10. Create a project:

        As a user
        I want to add data to a database
        So that i can insert a new project

11. Join a project:

        As a user
        I want to request access to the project's database
        So that i can be a member of the project

12. Leave a project:

        As a project-user
        I want to remove my user from the project's database
        So that i can leave the project

13. Modify project roles:
        
        As a project-owner
        I want to modify the project's database
        So that i can update accordingly the roles

14. View User-B-Personal-info :        

        As a user
        I want to access the user-B's database
        So that i can view User-B profile

15. Log in as a User:
        As a not logged user
        I want to access the user's database
        So that i can access the site

16. Invite a user to a project:

        As a project-owner 
        I want to add a user to the project's database
        So that i can invite a user to a project

17. Remove a user from a project:--------

        As a project-owner
        I want to remove a user from a project's database
        So that i can remove a user from the project

18. Modify project's description:

        As a project-owner
        I want to modify the project's database
        So that i can update the infos of the project

19. Add the link to git/drive:

        As a project-owner
        I want to add data to the project's database
        So that i can add an external link to the project 

20. Remove the link to git/drive:

        As a project-owner
        I want to remove data from the project's database
        So that i can remove an external link from the project

21. Modify the link to git/drive:

        As a project-owner
        I want to modify data from the project's database
        So that i can modify an external link from the project

22. Open mailbox:

        As a project-owner
        I want to add a mail-box to the project's database
        So that i can use the mailbox in the project page

23. Send a message to the mailbox:

        As a project-user
        I want to add data to the mailbox
        So that i can send a message to the mailbox

24. Remove a message to the mailbox: (su)----------------
  
        As a project-owner
        I want to remove data from the mailbox
        So that i can remove a message from the mailbox

25. Add restrictions:

        As a project-owner
        I want to add restriction to my project's database
        So that i can set restrictions in my project

26. Modify restrictions:

        As a project-owner
        I want to modify restrictions to my project's database
        So that i can modify restrictions in my project

27. Remove restrictions:

        As a project-owner
        I want to remove restrictions from my project's database
        So that i can remove restrictions from my projects

28. Add an evaluation to a project:

        As a user 
        I want to add data in the project's database
        So that i can add a review for the project

29. Add an evaluation to a member of the project:

        As a project-user
        I want to add data to a project-user's database
        So that i can add an evaluation for the user in the project

30. Add external comments in the project:

        As a user
        I want to add data to a project's database
        So that i can add comments in the project

31. Retrive User Data:
        As a not logged used
        I want to modify data in user's database
        So that i can retrive my password