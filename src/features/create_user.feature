Feature: I want to create "user" to site

        As a not-user
        I want to becoma a user
        So that i create ad account

Scenario: Creating new user
       Given I am on the home page
       When I follow "Sign up"
       When I fill in "user_name" with "Mario"
       And I fill in "user_surname" with "Rossi"
       And I fill in "user_email" with "mario.rossi@sdf.it"
       And I fill in "user_password" with "m"
       And I fill in "user_password_confirmation" with "m"
       And I press "Sign up"
       Then I should be on the home page
       And I should see "Mario"

