Feature: I want to destroy a  "project" 

        As a user
        I want to manage project db
        So that i destroy the project
           


Scenario: Destroy project

       
       Given I am on the home page
       # Then I follow "Logout"
       Then I follow "Log in"
       When I fill in "user_email" with "test@test.it"
       And I fill in "user_password" with "test"
       When I press "Log in"
       Then I should be on the home page
       #Given I am on the home page
       When I follow "Project"
       Then I should see "project to delete"
       Then I follow "View Project2"
       When I follow "Destroy Project"
       Then I should not see "partita di calcetto"


