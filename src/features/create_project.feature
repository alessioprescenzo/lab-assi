Feature: I want to add a  "project" to site

        As a user
        I want to add a project
        So that i create ad project
           


Scenario: Create project

       
       Given I am on the home page
       # Then I follow "Logout"
       Then I follow "Log in"
       When I fill in "user_email" with "test@test.it"
       And I fill in "user_password" with "test"
       When I press "Log in"
       Then I should be on the home page
       #Given I am on the home page
       When I follow "Profilo"
       When I fill in "project_name" with "Congresso di vienna"
       And I fill in "project_description" with "Un momento bello bello"
       And I fill in "project_linkesterno" with "localhost:3000"
       
       And I press "Create Project"
       
       Then I should see "Congresso di vienna"



