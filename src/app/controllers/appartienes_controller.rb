class AppartienesController< ApplicationController
    before_action :authenticate_user!
    before_action :require_permission, only: [:new, :create]

    

    def edit 
    end

    def new
        @user = User.friendly.find(params[:user_id])
        @project = Project.friendly.find(params[:project_id])
        @appartiene = Appartiene.new(user_params);         
        @appartiene.update(:admin => false);
        @user.appartienes << @appartiene
        redirect_to project_path(@project)
    end

    def index
    end
    
    def destroy
        @user = User.friendly.find(params[:user_id])
        @project = Project.friendly.find(params[:project_id])
        @appartiene = Appartiene.where("project_id = ? and user_id = ?",@project.id,@user.id).first()
        @appartiene.delete
        redirect_to project_path(@project)
    end

    protected
    def require_permission
        @appartiene = Appartiene.where("project_id = ? and user_id = ?",params[:project_id],current_user)
        if((@appartiene.any?))
            redirect_to user_path(current_user)
        end       
    end


    private 
    def user_params  #non so bene il perchè
        params.permit(:user_id,:project_id,:role)
    end
end