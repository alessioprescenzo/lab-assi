class ChatsController < ApplicationController

  before_action :authenticate_user!
  before_action :require_permission, only: [:new, :create, :show, :edit]

  def pRefresh
    render :partial => "chats/display"
  end

  def new           #da chiamare solo alla creazione del progetto
    @chat = Chat.new
  end

  def create        #da chiamare solo alla creazione del progetto
    @project = Project.friendly.find(params[:project_id])
    @chat = @project.chat.build(chat_params)
    if @chat.save
      flash[:success] = 'Chat added!'
      @project.chat_id= @chat.id
    else            #NO
      render 'new'
    end
	end
	
  def show
    @project = Project.friendly.find(params[:project_id])
    @chat = Chat.find(@project.chat_id)
	end

  def update
    @c = params[:chat]
    @project = Project.friendly.find(params[:project_id])
    @chat = Chat.find(@project.chat_id)
    if(@c["message_new"].length ==0)
      redirect_to project_chat_path(@chat.project_id, @chat)
    else
      @name = current_user.name
      @time = Time.now
      @old = @chat.message
      @chat.message = @old + "\n\n" + @name + " " + @current_user.surname + ': ' + @c["message_new"] + ' ;; ' + @time.to_s.truncate(19, omission: '')
      @chat.update(:message => @chat.message)
      redirect_to project_chat_path(@chat.project_id, @chat)
    end
  end
  
  protected_methods
  def require_permission
    @project = Project.friendly.find(params[:project_id])
    @appartienes = Appartiene.where(project_id: @project.id,user_id: current_user.id).first()
    if (!@appartienes || @appartienes.user_id != current_user.id)
      redirect_to ('/404')
    end
  end

  private

  def chat_params
    params.require(:project_id).permit(:project_id, :message, :message_new)
  end
  
  def new_send_params
    params.require(:chat).permit(:message, :message_new)
  end

end