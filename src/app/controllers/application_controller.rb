class ApplicationController < ActionController::Base
    add_flash_types :danger, :info, :warning, :success,:alert

    rescue_from CanCan::AccessDenied do |exception|
        redirect_to root_path, danger: "Not Authorized"
    end

    def not_found
        raise ActionController::RoutingError.new('Not Found')
    end
   
    protected  
    def after_sign_in_path_for(resource)
        if resource.provider == 'facebook' && resource.sign_in_count == 1
            edit_registration_path(current_user)
        else
            root_path
        end
    end
end
