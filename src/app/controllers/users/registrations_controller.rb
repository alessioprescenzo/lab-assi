# frozen_string_literal: true
class Users::RegistrationsController < Devise::RegistrationsController

  before_action :configure_account_update_params, only: [:update]
  before_action :configure_sign_up_params, only: [:create]

  def new 
    super
  end

  def create
    super
  end

  def edit
    super
  end
  
  # def update  
  #   super
  # end
  
  def destroy
    @projects = Project.where("user_id= ?" ,@user.id)
    @projects.destroy_all
    @user.destroy
    redirect_to root_path
  end

  protected
  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:surname,:email,:password,:datanascita,:luogonascita,:sesso,:skills,:role,:avatar])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name,:surname,:email,:password,:datanascita,:luogonascita,:sesso,:skills,:role,:avatar])
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
 
  protected

  def update_resource(resource, params)
    # Require current password if user is trying to change password.
    return super if params["password"]&.present? || params["email"] != resource.email
    # Allows user to update registration information without password.
    resource.update_without_password(params.except("current_password"))
  end

  private
  def user_params  #non so bene il perchè
      params.require(:user).permit(:name,:surname,:email,:password,:datanascita,:luogonascita,:sesso,:skills,:role,:avatar)
  end

end
