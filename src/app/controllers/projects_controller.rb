class ProjectsController < ApplicationController

    helper_method :commenta
    
    def commenta
            @c = params[:project]
            @project = Project.friendly.find(params[:project_id])            
            if(@c["message_new"].length == 0)
                redirect_to project_path(@project)
                flash[:alert] = "Comments must be at least 1 character long"
            else
                @name = current_user.name
                @time = Time.now
                @commmenti = @project.commenti + "\n\n" +  @name + " " + @current_user.surname+ ': ' + @c["message_new"] + ";;" + @time.to_s.truncate(19, omission: '') 
                @project.update(:commenti => @commmenti)
                redirect_to project_path(@project)
        end
    end

    before_action :authenticate_user!
    before_action :require_permission, only: [:destroy]

    def index
        @project=Project.all 
    end 

    def show
        @project = Project.friendly.find(params[:id])
    end

    def create
        @p=params[:project]
        @user = User.friendly.find(params[:user_id])
        if(@p["name"].length < 2)
            redirect_to user_path(@user)
            flash[:alert] = "Project's name must be at least 2 character long"
        else
            @project = @user.projects.create(project_params)
            @appartienes = Appartiene.where("project_id = ? and user_id = ?",@project.id,@user.id);
            @project.update(:user_id => @user.id, :appartienes_id => @project.id)
            @chat = @project.chat.create(params[:id])
            @appartienes.update(:admin => true)
            @project.update(:chat_id => @chat.id)

            redirect_to user_path(@user)
        end
    end

    def destroy
        @user = User.friendly.find(params[:user_id])
        @project = @user.projects.friendly.find(params[:project])
        @appartienes = Appartiene.where("project_id = ? and user_id = ?",@project.id,@user.id)
        @var = @project.appartienes_id
        @project.destroy
        redirect_to user_path(@user)
    end

    def edit
        @project = Project.friendly.find(params[:id])        
        if(Appartiene.where("project_id = ? and user_id = ? and admin = ?",@project.id,current_user.id,1).first() == nil)
            redirect_to project_path(@project), alert: "Not Authorized"
        end
    end

    def update #da sistemare
        @project = Project.friendly.find(params[:id])        
        if(Appartiene.where("project_id = ? and user_id = ? and admin = ?",@project.id,current_user.id,1).first() != nil)
            @new = params[:project]
            @project.update(id: @project.id, description: @new["description"], linkesterno: @new["linkesterno"], requisitipartecipazione: @new["requisitipartecipazione"], avatar: @new["avatar"])
            redirect_to project_path(@project), success: "Project modified"  
        else
            redirect_to project_path(@project), alert: "Not Authorized"
        end
    end

    protected
    def require_permission
        @user = User.friendly.find(params[:user_id])   
        if current_user != @user
            redirect_to ('/404')
        end
        @appartienes = Appartiene.where("project_id = ? and user_id = ?",params[:project],params[:user_id])
        if(@appartienes.any?)
            if(!@appartienes[0].admin)
                redirect_to ('/404')
            end 
        end
    end


    private
    def project_params
        params.require(:project).permit(:name,:description,:linkesterno,:requisitipartecipazione,:commenti,:chat,:message_new,:avatar)
    end
    def comment_params
        params.require(:project).permit(:commenti,:message_new,:commento)
    end
        
    def commenta_params
        params.require(:project).permit(:message, :message_new,:project_id)
    end

end
