class UsersController < ApplicationController
    
    helper_method :vote
    helper_method :promote

    def promote()
        authorize! :destroy, Users
        @user = User.friendly.find(params[:user])
        @user.update(:role => 'admin')
    end

    
    def vote() 
        @user = User.friendly.find(params[:user]);
        @project = Project.friendly.find(params[:id])
        if(Appartiene.where("project_id = ? and user_id = ?",@project.id,current_user.id).first() != nil)   
            @var =(@user.voto).to_i+(params[:voto]).to_i;
            @user.update( :voto => @var)
            if @project == nil
                redirect_to users_path
            else
                redirect_to project_path(@project)
            end
        else
            redirect_to project_path(@project), alert: "Not Authorized"
        end
    end
    
    before_action :authenticate_user!

    def index 
        @users=User.all
        authorize! :index, @users
    end

    def show
        @user = User.friendly.find(params[:id])    
    end
    
    def destroy
        authorize! :destroy, Users
        @user = User.friendly.find(params[:id])
        @user.destroy
        redirect_to users_path, notice: 'User deleted.'
    end


    private 
    def user_params
        params.require(:user).permit(:name,:surname,:email,:password,:datanascita,:luogonascita,:sesso,:skills,:role,:avatar)
    end 

end
