class ApplicationMailer < ActionMailer::Base
  default from: => 'laboratoriolabassi@gmail.com'
  layout 'mailer'
end
