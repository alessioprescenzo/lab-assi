# frozen_string_literal: true

class Ability
  include CanCan::Ability
  
  def initialize(user)
    user ||= User.new
    if user.role() == "admin"
      can :manage, :all 
    end
    if user.role() == "banned"
      cannot :manage, :all
      cannot :create, :all
      cannot :update, :all
      cannot :destroy, :all
    end
    if user.role() == "user"
      can :destroy, User
      can :create, :all      
      can :manage, Project
      can :read, User
      can :update, User
      cannot :destroy, :all
    end
    if user.role() == "moderator"
      
    end
  end
end
