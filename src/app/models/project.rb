class Project < ActiveRecord::Base
    extend FriendlyId
  
    friendly_id :name, use: :slugged
    has_many :appartienes
    has_many :user ,dependent: :destroy,:through => :appartienes
    has_many :chat, dependent: :destroy
    has_one_attached :avatar

    validates :name, length: {minimum: 2} 
    validates :description, length: {minimum: 2} 
    validates :requisitipartecipazione, length: {minimum: 2} 

    def listUser(id)
        @appartienes = Appartiene.where("project_id = ? ",id)
        @size = @appartienes.size
        utenti = Array.new
        while (@size>0)
            @size = @size - 1
            utenti.push(@appartienes[@size].user_id)
        end
        return utenti
    end

    def stampaUtenti(id)
        @users = User.where("id = ?",id)
        return @users[0]
    end

    def parsingComments(project)
        @size = project.commenti.size
        pComments = Array.new
        pComments = project.commenti.split("\n\n")
        pComments.shift(1)
        return pComments 
    end

end
