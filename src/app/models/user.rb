class User < ActiveRecord::Base
  extend FriendlyId
  require 'open-uri'
  friendly_id :completename, use: :slugged
  ROLES = %i[admin moderator user banned]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable,
         :omniauthable, :omniauth_providers => [:facebook]

  has_many :appartienes
  has_many :projects, dependent: :destroy, :through => :appartienes
  has_one_attached :avatar
  validates_length_of :skills, maximum: 400
  validates :name, presence: true,length: {minimum: 2, maximum: 256}
  validates :surname, presence: true,length: {minimum: 2, maximum: 256}
  validates :luogonascita, presence: true,length: {minimum: 2, maximum: 256}
  validates :skills, presence: true,length: {minimum: 2, maximum: 256}
 
  def initialize(role_id = 0)
    @role = ROLES.include?(role_id) ? ROLES[role_id] : ROLES[0]   
    super
  end

  def completename
    "#{name}#{surname}"
  end
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.name = auth.info.name.split(" ").first  
      user.surname = auth.info.name.split(" ").last 
      image = open(auth.info.image)
      user.avatar.attach(io: image,filename: "#{user.name}#{user.surname}.jpg")
      user.sesso = auth.info.gender
      user.skills = "Enter your skills"
      user.luogonascita = "Enter your hometown"
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
end
