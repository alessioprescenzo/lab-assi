class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :description, default: "My Nice job"
      t.references :user, foreign_key: true
      t.references :appartienes

      t.string :linkesterno, default: ""
      t.string :requisitipartecipazione , default: "Aperto a tutti"
      t.string :commenti, default: ""
      
      t.references :chat, foreign_key: false
      t.string :message_new, :default => ""
      t.timestamps
    end
  end
end
