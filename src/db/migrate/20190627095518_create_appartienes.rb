class CreateAppartienes < ActiveRecord::Migration[5.2]
  def change
    create_table :appartienes do |t|
      t.references :user, foreign_key: true
      t.references :project, foreign_key: true
      t.boolean :admin, :default => false

      t.timestamps
    end
  end
end
