class CreateChats < ActiveRecord::Migration[5.2]
  def change
    create_table :chats do |t|
      t.references :project, foreign_key: true
      t.string :message, :default => "  "
      t.string :message_new, :default => ""
      t.timestamps
    end
  end
end
