# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.create([{name: 'luigi', surname: 'verdi', email: 'luigi.verdi@sdf.it', password: 'test', password_confirmation: 'test'},{name: 'Rick', surname: 'Ranchez', email: 'test@test.it', password: 'test', password_confirmation: 'test'}])
project =  Project.create([{name: 'partita di calcetto', user_id: 1},{name: 'project to delete', user_id: 2}])
appartiene = Appartiene.create([{user_id: 1, project_id: 1, admin:1},{user_id: 2, project_id: 2, admin: 1}])