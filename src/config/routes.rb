Rails.application.routes.draw do
  
  devise_for :users, :controllers => {
    :sessions => 'users/sessions',
    :registrations => 'users/registrations',
    :passwords => 'users/passwords',
    :mailer => 'users/mailer',
    :confirmations => 'users/confirmations',
    :unlocks => 'users/unlocks',
    :shared => 'users/shared',
    :omniauth_callbacks => 'users/omniauth_callbacks'
  }
  resources :users do
    resources :projects
    resources :appartienes
  end

  resources :projects do
    resources :chats
  end
  
  devise_scope :user do
    delete 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_sessions
  end
  
  
  root 'root#index'
  post '/votes/:user,:voto', to: 'users#vote', as: 'vote'
  post '/users/:user', to: 'users#promote', as: 'promote'
  patch '/commenta/:project_id,:commento', to: 'projects#commenta', as: 'commenta'
  patch '/projects/:project_id/chats/:chat/edit', to: 'chats#edit', as: 'send'
end
